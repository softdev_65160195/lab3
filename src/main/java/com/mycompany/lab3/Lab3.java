/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab3;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab3 {
    static char currentPlayer = 'X';
    static int row, col;
    static boolean gameFin = false;
    static boolean gameCon = true;
    static boolean correct = false;
    static boolean pass = true;
    static char[][] board = {{'-','-','-'},
                             {'-','-','-'},
                             {'-','-','-'}};
    
    public static void Welcome(){
        System.out.println("Welcome to XO Game!");
    }

    public static void Board(){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                System.out.print(board[i][j]+ " ");
            }
            System.out.println();
        }
    }
    
    public static void inputChar(){
        while(correct == false){
            Scanner kb = new Scanner(System.in);
            System.out.print("Enter Your row col: ");
            row = kb.nextInt();
            col = kb.nextInt();
            if(board[row-1][col-1] != '-'){
                System.out.println("Move Failed!!");
                Board();
                System.out.println("Player "+ currentPlayer + " Turn");
            }else{
                board[row-1][col-1] = currentPlayer;
                break;
            }
        }
    }
    
    public static boolean checkWin(char [][] board,char currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == currentPlayer && board[i][0] == board[i][1] && board[i][1] == board[i][2]) {
                return true;
            }
        }
        for (int j = 0; j < 3; j++) {
            if (board[0][j] == currentPlayer && board[0][j] == board[1][j] && board[1][j] == board[2][j]) {
                return true;
            }
        }

        if (board[0][0] == currentPlayer && board[0][0] == board[1][1] && board[1][1] == board[2][2]) {
            return true;
        }
        if (board[0][2] == currentPlayer && board[0][2] == board[1][1] && board[1][1] == board[2][0]) {
            return true;
        }
        return false;
    }
    
    public static boolean checkDraw(char [][] board){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                if(board[i][j] == '-'){
                    return false;
                }
            }
        }
        return true;
    }
    
    public static void switchPlayer(){
        if(currentPlayer == 'X'){
            currentPlayer = 'O';
        }else{
            currentPlayer = 'X';
        }
    }
    
    public static void printTurn(){
        System.out.println("Player "+ currentPlayer + " Turn");
    }   
    
    public static void reBoard(){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                board[i][j] = '-';
            }
        }
    }
    
    public static void main(String[] args) {
        Welcome();
        while(gameCon == true){
            while (gameFin == false){
                Board();
                printTurn();
                inputChar();
                if (checkWin(board,currentPlayer)) {
                    Board();
                    System.out.println("Player " + currentPlayer + " Win!");
                    gameFin = true;
                } else if (checkDraw(board)) {
                    Board();
                    System.out.println("2 Player is Draw!");
                    gameFin = true;
                } else {
                    switchPlayer();
                }
                pass = true;
            }
            while(pass == true){
                Scanner kb = new Scanner(System.in);
                System.out.println("Continue? (y/n): ");
                char Ans = kb.next().charAt(0);
                if(Ans == 'y' || Ans == 'Y'){
                    pass = false;
                    gameCon = true;
                    gameFin = false;
                    reBoard();
                }else if(Ans == 'n' || Ans == 'N'){
                    pass = false;
                    gameCon = false;
                }else{
                    System.out.println("Please Try Agian");
                }
            }
        }
    }
}
