/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author boat5
 */
public class JUnitTestLab3 {
    
    public JUnitTestLab3() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    //CheckWin
    @Test
    public void testCheckWin_X_Verticalrow1_output_true(){
        char[][] board = {{'X','X','X'},
                          {'-','-','-'},
                          {'-','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticalrow2_output_true(){
        char[][] board = {{'-','-','-'},
                          {'X','X','X'},
                          {'-','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Verticalrow3_output_true(){
        char[][] board = {{'-','-','-'},
                          {'-','-','-'},
                          {'X','X','X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Horizontal1_output_true(){
        char[][] board = {{'X','-','-'},
                          {'X','-','-'},
                          {'X','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Horizontal2_output_true(){
        char[][] board = {{'-','X','-'},
                          {'-','X','-'},
                          {'-','X','-'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Horizontal3_output_true(){
        char[][] board = {{'-','-','X'},
                          {'-','-','X'},
                          {'-','-','X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticalrow1_output_true(){
        char[][] board = {{'O','O','O'},
                          {'-','-','-'},
                          {'-','-','-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticalrow2_output_true(){
        char[][] board = {{'-','-','-'},
                          {'O','O','O'},
                          {'-','-','-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticalrow3_output_true(){
        char[][] board = {{'-','-','-'},
                          {'-','-','-'},
                          {'O','O','O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Diagonal1_output_true(){
        char[][] board = {{'X','-','-'},
                          {'-','X','-'},
                          {'-','-','X'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_X_Diagonal2_output_true(){
        char[][] board = {{'-','-','X'},
                          {'-','X','-'},
                          {'X','-','-'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Diagonal1_output_true(){
        char[][] board = {{'-','-','O'},
                          {'-','O','-'},
                          {'O','-','-'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Diagonal2_output_true(){
        char[][] board = {{'O','-','-'},
                          {'-','O','-'},
                          {'-','-','O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Diagonal_output_false(){
        char[][] board = {{'X','-','-'},
                          {'-','O','-'},
                          {'-','X','O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWin_X_Diagonal_output_false(){
        char[][] board = {{'O','X','-'},
                          {'-','O','-'},
                          {'-','X','O'}};
        char currentPlayer = 'X';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(false,result);
    }
    //CheckDraw
    @Test
    public void testCheckDraw_output_false(){
        char[][] board = {{'O','-','O'},
                          {'-','X','X'},
                          {'-','-','O'}};
        boolean result = Lab3.checkDraw(board);
        assertEquals(false,result);
    }
    @Test
    public void testCheckDraw_output_true(){
        char[][] board = {{'O','X','O'},
                          {'O','X','X'},
                          {'X','O','O'}};
        boolean result = Lab3.checkDraw(board);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWin_O_Verticalrow3_output_false(){
        char[][] board = {{'X','-','-'},
                          {'-','O','-'},
                          {'X','O','O'}};
        char currentPlayer = 'O';
        boolean result = Lab3.checkWin(board,currentPlayer);
        assertEquals(false,result);
    }
}
